//soal 1
function luas(p, l) {
    return "luas = " + p * l + " ,keliling = " + 2*(p + l)
}

// Driver code
console.log(luas(8, 2));// contoh p=8, l=2


//soal 2
const firstName = "William"
const lastName = "Imoh"
const fullName = `${firstName} ${lastName}`
const William = {fullName}

// Driver code
console.log(William)


//soal 3
var newObject = {
    namaDepan: "Muhammad",
    namaBelakang: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {namaDepan, namaBelakang, address, hobby} = newObject

// Driver code
console.log(namaDepan, namaBelakang, address, hobby)


//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]

//Driver Code
console.log(combined)

//soal 5
const a = "lorem "
const view = "glass " 
const b = "dolor sit amet, "
const c = "consectetur adipiscing elit,"
const planet = "earth" 

var before = `${a} ${view} ${b} ${c} ${planet}`

//Driver Code
console.log(before)


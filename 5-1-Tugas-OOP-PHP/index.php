<?php

interface InfoHewan {
    public function getInfohewan();
}

class Hewan {
    private $nama,
            $darah,
            $jumlahKaki,
            $keahlian;

    public function __construct($nama = "nama", $darah = 50, $jumlahKaki = "jumlahKaki", $keahlian = "keahlian") {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }

    abstract public function atraksi();
}

class Fight {
    private $attackPower,
            $defencePower;

    public function __construct($attackPower = "attactPower", $defencePower = "defencePower") {
        $this->attactPower = $attackPower;
        $this->defencePower = $defencePower;
    }

    abstract public function serang();

    abstract public function diserang();
}

class Elang extends Hewan implements InfoHewan {
    public $nama = "Elang";
    public $keahlian = "terbang tinggi";
}

class Harimau extends Hewan implements InfoHewan {
    public $nama = "harimau";
    public $keahlian = "lari cepat"
}

class 

$elang1 = new Elang();
$harimau1 = new Harimau();


?>